// import
const express = require('express')
const bcrypt = require('bcrypt')

// initialize
const app = express()
const port = 3000

// middleware
app.use(express.json())

// local array as simulated test database
const users = []

// route: get user from simulated database
app.get('/users/get', (req, res) => {
  res.json(users)
})

// route: create new user into simulated database
app.post('/users/create', async (req, res) => {
  try {
    // encrypt password
    const salt = await bcrypt.genSalt(10)
    const hashedPassword = await bcrypt.hash(req.body.password, salt)

    // initialize
    const user = {
      name: req.body.name,
      password: hashedPassword
    }

    // array: push user into simulated database
    users.push(user)

    res.status(201).send('User is created')
  } catch (err) {
    res.status(500).send('User is not created!')
  }
})

// route: log user into page
app.post('/users/login', async (req, res) => {
  // function: search for user in simulated database
  const user = users.find(user => (user.name = req.body.name))

  // statement: check if user is not empty
  if (user === null) return res.send(400).status('Cannot find user!')

  try {
    // statement: check if password is similar
    if (await bcrypt.compare(req.body.password, user.password)) {
      res.status(200).send('Successfull logged in')
    } else {
      res.status(200).send('Not logged in!')
    }
  } catch (err) {
    res.status(404).send()
  }
})

// listen to server port
app.listen(port, () => {
  console.log(`API is running on port ${port}`)
})
